import React from 'react';
import {StyleSheet, TouchableOpacity, View, Image} from 'react-native';

export const AddButton = ({onClick}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => onClick()} style={styles.wrap} activeOpacity={0.9}>
        <Image style={styles.icon} source={require('app/assets/plus.png')} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 65,
    height: 65,
    borderWidth: 4,
    borderColor: '#fff',
    borderRadius: 32,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2,
  },
  wrap: {
    width: 60,
    height: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#f4f4f4',
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#D3D3D3',
    shadowOpacity: 1,
    elevation: 7,
  },
  icon: {
    width: 30,
    height: 30,
  },
});
