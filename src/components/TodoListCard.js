import React, {useState} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import CircleCheckBox from 'react-native-circle-checkbox';

export const TodoListCard = ({
  children,
  imagePath,
  onCompleteClick,
  onItemClick,
}) => {
  return (
    <View style={styles.cardWrap}>
      <View style={{width: '15%'}}>
        <CircleCheckBox
          checked={false}
          onToggle={onCompleteClick}
          outerColor="gray"
          filterColor="white"
          innerColor="gray"
        />
      </View>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '85%',
        }}
        onPress={onItemClick}
        activeOpacity={0.7}>
        <Text
          numberOfLines={1}
          ellipsizeMode="tail"
          style={{...styles.text, width: imagePath ? '70%' : '90%'}}>
          {children}
        </Text>
        {imagePath ? <Image source={imagePath} style={styles.image} /> : null}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  cardWrap: {
    paddingHorizontal: 20,
    width: '100%',
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    borderStyle: 'solid',
    borderRadius: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#f4f4f4',
  },
  image: {
    width: 90,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
  },
  text: {
    fontSize: 18,
    lineHeight: 22,
  },
});
