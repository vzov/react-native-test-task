import React, {useState} from 'react';
import {View, Text, StyleSheet, FlatList, StatusBar} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {TodoListCard} from 'app/src/components/TodoListCard';
import {AddButton} from 'app/src/components/AddButton';

export const TodoListScreen = ({navigation, route}) => {
  const [todos, setTodos] = useState([]);

  React.useEffect(() => {
    if (route.params?.item) {
      const todo = route.params.item;

      const found = todos.find(item => item.id === todo.id);
      if (!found) {
        setTodos(todos => [...todos, todo]);
      } else {
        setTodos(todos.map(item => (item.id === todo.id ? todo : item)));
      }
    }
  }, [route.params?.item]);

  const onCreateTaskClick = () => {
    navigation.navigate('Item');
  };

  const onUpdateTaskClick = item => {
    navigation.navigate('Item', {item: item});
  };

  const onCompleteTaskClick = itemId => {
    setTodos(todos.filter(item => item.id !== itemId));
  };

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Text style={styles.title}>React Native</Text>

      {todos.length > 0 ? (
        <FlatList
          style={{width: '100%', marginBottom: 70}}
          data={todos}
          renderItem={({item}) => (
            <TodoListCard
              id={item.id.toString()}
              imagePath={item.imagePath}
              onItemClick={() => onUpdateTaskClick(item)}
              onCompleteClick={() => onCompleteTaskClick(item.id)}>
              {item.text}
            </TodoListCard>
          )}
          keyExtractor={item => item.id}
        />
      ) : (
        <Text style={styles.noTasksText}>No tasks</Text>
      )}

      <View style={styles.bottomMenuWrap}>
        <LinearGradient
          colors={['transparent', '#f4f4f4']}
          style={{width: '100%', height: 12}}
        />
        <View style={styles.bottomMenu}>
          <View style={styles.button}>
            <AddButton onClick={onCreateTaskClick} />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 40,
  },
  title: {
    fontSize: 35,
    marginVertical: 15,
  },
  noTasksText: {
    fontSize: 20,
    marginTop: 40,
  },
  button: {
    bottom: 35,
    borderColor: 'white',
    borderWidth: 2,
  },
  bottomMenu: {
    height: 70,
    backgroundColor: 'white',
    width: '105%',
    alignItems: 'center',
    bottom: 0,
  },
  bottomMenuWrap: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
});
