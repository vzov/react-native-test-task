import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';

export const CreateItemScreen = ({navigation, route}) => {
  const item = route.params?.item;

  const [text, setText] = useState(item != null ? item.text : '');
  const [imagePath, setImagePath] = useState(
    item != null ? item.imagePath : null,
  );

  const onLoadImageClick = () => {
    const options = {
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.launchImageLibrary(options, response => {
      if (!response.didCancel && !response.error) {
        setImagePath({uri: response.uri});
      }
    });
  };

  const onDeleteImageClick = () => {
    setImagePath(null);
  };

  const onSaveClick = () => {
    const newItem = {
      text: text,
      id: item != null ? item.id : Date.now(),
      imagePath: imagePath,
    };

    navigation.navigate('Home', {item: newItem});
  };

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <View style={styles.header}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => navigation.goBack()}>
            <Image
              style={styles.close}
              source={require('app/assets/close.png')}
            />
          </TouchableOpacity>
          <Text style={styles.title}>
            {item == null ? 'Add Task' : 'Edit Task'}
          </Text>
        </View>
        {text !== '' ? (
          <TouchableOpacity activeOpacity={0.7} onPress={() => onSaveClick()}>
            <Text style={{...styles.button, backgroundColor: '#33CCCC'}}>
              Save
            </Text>
          </TouchableOpacity>
        ) : (
          <Text style={{...styles.button, backgroundColor: 'gray'}}>Save</Text>
        )}
      </View>

      <View style={{width: '100%', alignItems: 'center'}}>
        <TextInput
          style={styles.input}
          placeholder="Task"
          multiline
          maxLength={255}
          value={text}
          autoFocus={true}
          onChangeText={setText}
        />
      </View>

      <View style={styles.images}>
        {imagePath ? (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => onDeleteImageClick()}>
            <Image style={styles.image} source={imagePath} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => onLoadImageClick()}>
            <Text
              style={styles.loadImageButton}
              source={require('app/assets/image.png')}>
              Load Image
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  header: {
    height: 100,
    backgroundColor: '#f8f8f8',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    padding: 16,
  },
  title: {
    fontSize: 20,
    margin: 'auto',
  },
  close: {
    width: 30,
    height: 30,
    marginRight: 15,
  },
  input: {
    width: '90%',
    height: '70%',
    textAlignVertical: 'top',
    paddingHorizontal: 15,
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 1,
  },
  button: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 5,
    fontSize: 16,
    color: 'white',
  },
  images: {
    flexDirection: 'row',
    width: '90%',
    position: 'absolute',
    bottom: 0,
    height: 100,
    alignItems: 'center',
    justifyContent: 'flex-start',
    zIndex: 2,
    backgroundColor: 'white',
  },
  image: {
    width: 90,
    height: 90,
    resizeMode: 'contain',
  },
  loadImageButton: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 5,
    fontSize: 16,
    borderColor: '#33CCCC',
    borderWidth: 2,
    color: 'grey',
  },
});
