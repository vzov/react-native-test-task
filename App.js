import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {TodoListScreen} from 'app/src/screens/TodoListScreen';
import {CreateItemScreen} from 'app/src/screens/CreateItemScreen'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="Home"
          component={TodoListScreen}
        />
        <Stack.Screen
          name="Item"
          component={CreateItemScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
